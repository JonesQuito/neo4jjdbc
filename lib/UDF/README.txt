-- Diego Mendes
-- Instruções para criar a UDF de distancia de adição no PostgreSQL

Para inserir a UDF no PostgreSQL, você deve compilar o arquivo "edit_distance.c"
utilizando o Makefile. Para isso, basta navegar pelo terminal até a pasta onde se encontra o
arquivo "edit_distance.c", e digitar, como root:

# make

Depois, basta acessar o PostgreSQL por linha de comando, digitando:

$ psql <DBNAME>   

onde <DBNAME> deve ser substituído pelo nome do banco de dados, ou deixado em branco.

Feito isso, segue a sintaxe dos comandos para criar as funções no SGBD:

CREATE OR REPLACE FUNCTION edit_distance(text,text) 
RETURNS integer                                     
as 'edit_distance.so', 
'edit_distance' LANGUAGE 'C';

CREATE OR REPLACE FUNCTION edit_distance(text,text,integer) 
RETURNS bool
as 'edit_distance.so', 
'edit_distance_bool' LANGUAGE 'C';

Pode ser que, dependendo de sua distribuição linux, onde encontra-se 
escrito "edit_distance.so", você deverá colocar apenas "edit_distance", sem a
extensão ".so".

Pronto, sua UDF já estará pronta para ser utilizada.
