#include "postgres.h"
#include "fmgr.h"

PG_MODULE_MAGIC;

//typedef int int32;

Datum	edit_distance(PG_FUNCTION_ARGS);
int minimum (int,int,int);

PG_FUNCTION_INFO_V1(edit_distance_bool);
PG_FUNCTION_INFO_V1(edit_distance);



// Recebe duas strings e retorna a edit distance entre as duas strings
Datum edit_distance (PG_FUNCTION_ARGS) {
	
	// Pegando os dois argumentos da funcao
	text* string1 = PG_GETARG_TEXT_P(0);	
	text* string2 = PG_GETARG_TEXT_P(1);
	
	// Copiando as strings passadas como argumento para as variaveis "s" e "t"
	char *s = VARDATA(string1);
	char *t = VARDATA(string2);
	
	// Pegando o tamanho das funcoes
	int 	m = (VARSIZE(string1) - VARHDRSZ), 
			n = (VARSIZE(string2) - VARHDRSZ), 
			i, j;
	
	// Criando a matriz "d" e inicializando-a 
	int d [m+1][n+1];

	// Variavel que armazenara a distancia de adicao entre as strings
	int edit_distance;
	
	for (i=0 ; i <= m; i++)
		d[i][0] = i;

	for (j=0 ; j <= n; j++)
		d[0][j] = j;
	
	
	// Calculando a distancia de adicao entre as strings	
	for (j=1 ; j <= n ; j++) {
		
		for (i=1 ; i <= m ; i++) {
			
			if (s[i-1] == t[j-1]) 
				d[i][j] = d[i-1][j-1];
				
			else 
				d[i][j] = minimum (
								d[i-1][j] + 1 ,
								d[i][j-1] + 1,
								d[i-1][j-1] + 1 
							 );
		}	
	}
	
	PG_RETURN_INT32(d[m][n]);

}


// Recebe 3 argumentos : string1, string2 e a edit distance
// Se a edit distance entre as strings for menor ou igual ao valor passado, retorna true
// Em caso contrário, retorna falso
Datum edit_distance_bool (PG_FUNCTION_ARGS) {
	
	// Pegando os dois argumentos da funcao
	text* string1 = PG_GETARG_TEXT_P(0);	
	text* string2 = PG_GETARG_TEXT_P(1);
	int dist_max = PG_GETARG_INT32(2);
	
	// Copiando as strings passadas como argumento para as variaveis "s" e "t"
	char *s = VARDATA(string1);
	char *t = VARDATA(string2);
	
	// Pegando o tamanho das funcoes
	int 	m = (VARSIZE(string1) - VARHDRSZ), 
			n = (VARSIZE(string2) - VARHDRSZ), 
			i, j;
	
	// Criando a matriz "d" e inicializando-a 
	int d [m+1][n+1];

	// Variavel que armazenara a distancia de adicao entre as strings
	int edit_distance;
	
	for (i=0 ; i <= m; i++)
		d[i][0] = i;

	for (j=0 ; j <= n; j++)
		d[0][j] = j;
	
	
	// Calculando a distancia de adicao entre as strings	
	for (j=1 ; j <= n ; j++) {
		
		for (i=1 ; i <= m ; i++) {
			
			if (s[i-1] == t[j-1]) 
				d[i][j] = d[i-1][j-1];
				
			else 
				d[i][j] = minimum (
								d[i-1][j] + 1 ,
								d[i][j-1] + 1,
								d[i-1][j-1] + 1 
							 );
		}	
	}
	
	edit_distance = d[m][n];

	if (edit_distance <= dist_max)
		PG_RETURN_BOOL(true);
	else
		PG_RETURN_BOOL(false);

}

// Retorna o menor entre os tres valores passados
int minimum (int a, int b, int c) {
	
	int minimo = -1;
	
	if (a <= b && a <= c) 
		minimo = a;
	else if (b <= a && b <= c)
		minimo = b;
	else 
		minimo = c;
	
	return minimo;
}

