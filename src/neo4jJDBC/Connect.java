package neo4jJDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {

	private static Connection connection = null;

	private Connect() throws Exception {
		try {
			connection = DriverManager.getConnection("jdbc:neo4j:bolt://localhost", "neo4j", "123456");
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public static Connection getConnection() throws Exception {
		if (connection == null) {
			new Connect();
		}
		return connection;
	}

	public static boolean has_connection() {
		return connection != null;
	}

	public static void close() throws SQLException {
		if (has_connection()) {
			connection.close();
		}
	}
}
