package neo4jJDBC;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;


public class Principal {

	public static void main(String[] args) {
		//plan=relationalExtractor conf=dblp.conf overwrite=true printResult=true
		String argumentos[] = {"plan=relationalExtractor", "conf=dblp.conf", "overwrite=true", "printResult=true"};
		



		Connection con = null;
		try {
			con = Connect.getConnection();
			String query = "MATCH (p:Produto) RETURN p";
			ProdutoMatch pm = new ProdutoMatch(con);
			ArrayList<Produto> produtos = (ArrayList<Produto>)pm.getProdutos(query);
			
			for(Produto p: produtos) {
				System.out.println(p);
			}
	
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				System.out.println("Error: " + e.getMessage());
			}
		}
	}
}
