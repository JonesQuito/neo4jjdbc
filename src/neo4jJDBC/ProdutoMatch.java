package neo4jJDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ProdutoMatch {
	
	private List<Produto> produtos = new ArrayList<>();
	private Connection con;
	

	public ProdutoMatch(Connection con) {
		this.con = con;
	}
	
	public List<Produto> getProdutos(String query) throws SQLException{
		Gson gson = new GsonBuilder().create();
		
		try (Statement stmt = con.createStatement()) {
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				Produto p = gson.fromJson(rs.getString(1), Produto.class);
				produtos.add(p);
			}
		}
		
		return this.produtos;
	}
}
